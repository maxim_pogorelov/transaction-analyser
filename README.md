# transaction-analyser


How to build:

- Make sure you have Java 1.8 installed
- Make sure you have Maven installed
- Go to the root of the project and run: mvn clean install
- In the target folder you will find transaction-analyser-1.0-jar-with-dependencies.jar
- You can also find already working .jar file in the build folder

You can specify filters: 

- fromDate
- toDate
- merchant

Example: "fromDate: 20/08/2018 12:00:00" "toDate: 20/08/2018 13:00:00" "merchant: Kwik-E-Mart"

Note: please do not forget to put every single filter in quotes ""

How to run:

- java -jar target/transaction-analyser-1.0-jar-with-dependencies.jar {AbsolutePathToTheCSVFile}
- java -jar target/transaction-analyser-1.0-jar-with-dependencies.jar {AbsolutePathToTheCSVFile} "fromDate: 20/08/2018 12:00:00" "toDate: 20/08/2018 13:00:00" "merchant: Kwik-E-Mart"


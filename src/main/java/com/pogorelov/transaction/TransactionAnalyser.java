package com.pogorelov.transaction;

import com.pogorelov.transaction.model.Transaction;
import com.pogorelov.transaction.model.TransactionsStatistic;
import com.pogorelov.transaction.service.ExtractDataService;
import com.pogorelov.transaction.service.InputStreamService;
import com.pogorelov.transaction.service.ProcessTransactionService;
import com.pogorelov.transaction.service.filter.PredicateService;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Predicate;

@Slf4j
public class TransactionAnalyser {

	public static void main(String[] args) throws Exception {
		if (args.length < 1)
			throw new UnsupportedOperationException("Please check arguments");

		String filePath = args[0];

		ProcessTransactionService transactionService = new ProcessTransactionService(new InputStreamService(),
				new ExtractDataService());

		PredicateService predicateService = new PredicateService();

		Predicate<Transaction> filter = predicateService.buildFilter(args);

		TransactionsStatistic transactionsStatistic = transactionService.process(filePath, filter);

		System.out.println(transactionsStatistic.toString());
		log.info(transactionsStatistic.toString());
	}
}

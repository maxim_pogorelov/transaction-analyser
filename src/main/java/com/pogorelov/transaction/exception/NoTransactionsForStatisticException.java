package com.pogorelov.transaction.exception;


public class NoTransactionsForStatisticException extends RuntimeException {
    public NoTransactionsForStatisticException(String message) {
        super(message);
    }
}

package com.pogorelov.transaction.exception;



public class TransactionParseException extends RuntimeException {

    public TransactionParseException(Throwable throwable) {
        super(throwable);
    }
}

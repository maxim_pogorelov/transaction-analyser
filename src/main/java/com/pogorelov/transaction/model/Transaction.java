package com.pogorelov.transaction.model;


import jdk.nashorn.internal.ir.annotations.Immutable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
@Immutable
@AllArgsConstructor
public class Transaction {

    private String id;
    private LocalDateTime date;
    private Double amount;
    private String merchant;
    private TransactionType type;
    private String relatedTransaction;

}

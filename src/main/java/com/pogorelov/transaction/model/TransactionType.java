package com.pogorelov.transaction.model;

public enum TransactionType {
    PAYMENT, REVERSAL
}

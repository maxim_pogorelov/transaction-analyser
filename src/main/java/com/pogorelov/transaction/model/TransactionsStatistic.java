package com.pogorelov.transaction.model;


import jdk.nashorn.internal.ir.annotations.Immutable;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@Immutable
public class TransactionsStatistic {

    private Integer numberOfTransactions;
    private Double average;

    @Override
    public String toString() {
        return String.format("Number of transactions = %d\nAverage Transaction Value = %.2f\n",
                this.numberOfTransactions, this.average);
    }
}

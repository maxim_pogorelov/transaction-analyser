package com.pogorelov.transaction.service;

import com.google.common.collect.Lists;
import com.pogorelov.transaction.exception.TransactionParseException;
import com.pogorelov.transaction.model.Transaction;
import com.pogorelov.transaction.model.TransactionType;
import com.pogorelov.transaction.util.ExecutorUtil;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.pogorelov.transaction.util.StringUtils.isNotEmpty;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;


@Slf4j
public class ExtractDataService {

	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
	private static final int DEFAULT_THREADS_NUMBER = 10;
	private static final int DEFAULT_PARTITION_SIZE = 10;
	private static final String COMMA = ",";

	public List<Transaction> extractTransactions(List<String> lines) {
		ExecutorService executor = createExecutorService();

		try {
			List<List<String>> partitions = Lists.partition(lines, DEFAULT_PARTITION_SIZE);

			Set<CompletableFuture<List<Transaction>>> completableFutures = partitions.stream()
					.map(partition -> CompletableFuture.supplyAsync(() -> partition.stream()
											.map(mapToTransaction)
											.collect(Collectors.toList()),
									executor)
							.exceptionally(error -> {
								log.error("Failed parse transaction {}", error);
								throw new TransactionParseException(error);
							}))
					.collect(toSet());

			return completableFutures.stream()
					.map(CompletableFuture::join)
					.flatMap(List::stream)
					.collect(toList());

		} finally {
			ExecutorUtil.shutdownExecutor(executor);
		}
	}

	private Function<String, Transaction> mapToTransaction = (line) -> {
		String[] t = line.split(COMMA);

		return Transaction.builder()
				.id(parseString(t[0]))
				.date(parseDate(t[1]))
				.amount(parseAmount(t[2]))
				.merchant(parseString(t[3]))
				.type(parseType(t[4]))
				.relatedTransaction(t.length > 5 ? parseString(t[5]) : null)
				.build();
	};

	private Double parseAmount(String s) {
		return isNotEmpty(s) ? Double.valueOf(s.trim()) : null;
	}

	public static LocalDateTime parseDate(String s) {
		return isNotEmpty(s) ? LocalDateTime.parse(s.trim(), DATE_TIME_FORMATTER) : null;
	}

	public static String parseString(String s) {
		return isNotEmpty(s) ? s.trim() : null;
	}

	private TransactionType parseType(String s) {
		return isNotEmpty(s) ? TransactionType.valueOf(s.trim()) : null;
	}

	private ExecutorService createExecutorService() {
		return createExecutorService(DEFAULT_THREADS_NUMBER);
	}

	private ExecutorService createExecutorService(int nThreads) {
		return Executors.newFixedThreadPool(nThreads);
	}

}

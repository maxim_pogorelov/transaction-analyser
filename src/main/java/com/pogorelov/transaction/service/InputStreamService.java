package com.pogorelov.transaction.service;


import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Slf4j
public class InputStreamService {

    public List<String> getLines(String path) {
        List<String> lines = emptyList();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(getInputStream(path)))) {

            lines = br.lines()
                    .skip(1)
                    .collect(Collectors.toList());

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("InputOutput exception: {}", e.getMessage());
        }

        return lines;
    }

    InputStream getInputStream(String path) throws IOException {
        log.info("Find input stream using path {}", path);
        File file = Paths.get(path).toFile();
        return new FileInputStream(file);
    }
}

package com.pogorelov.transaction.service;

import com.pogorelov.transaction.exception.NoTransactionsForStatisticException;
import com.pogorelov.transaction.model.Transaction;
import com.pogorelov.transaction.model.TransactionsStatistic;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
public class ProcessTransactionService {

	private final InputStreamService inputStreamService;
	private final ExtractDataService extractDataService;

	public ProcessTransactionService(InputStreamService inputStreamService, ExtractDataService extractDataService) {
		this.inputStreamService = inputStreamService;
		this.extractDataService = extractDataService;
	}

	public TransactionsStatistic process(String filePath, Predicate<Transaction> filter) {
		List<String> lines = inputStreamService.getLines(filePath);

		List<Transaction> transactions = extractDataService.extractTransactions(lines);

		return generateStatistic(transactions, filter);
	}

	private TransactionsStatistic generateStatistic(List<Transaction> transactions, Predicate<Transaction> predicate) {

		List<Transaction> foundTransactions = transactions.stream()
				.filter(predicate)
				.collect(Collectors.toList());

		Double average = foundTransactions.stream()
                .mapToDouble(Transaction::getAmount)
                .average()
				.orElseThrow(() -> new NoTransactionsForStatisticException("Could not calculate average"));

		return TransactionsStatistic.builder()
                .numberOfTransactions(foundTransactions.size())
                .average(average)
				.build();
	}
}

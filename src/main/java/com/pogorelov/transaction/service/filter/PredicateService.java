package com.pogorelov.transaction.service.filter;

import com.pogorelov.transaction.model.Transaction;
import com.pogorelov.transaction.model.TransactionType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static com.pogorelov.transaction.service.ExtractDataService.parseDate;
import static com.pogorelov.transaction.service.ExtractDataService.parseString;

public class PredicateService {


	public Predicate<Transaction> buildFilter(String... args) {
        
        List<Predicate<Transaction>> predicates = new ArrayList<>();
        predicates.add(notReversal());

        for (int i = 1; i < args.length; i++) {
            String filterParam = args[i];
            String[] parts = filterParam.split("^.+?:");

            if (filterParam.contains("fromDate")) {
                predicates.add(fromDate(parseDate(parts[1])));
            } else if (filterParam.contains("toDate")) {
                predicates.add(toDate(parseDate(parts[1])));
            } else if (filterParam.contains("merchant")) {
                predicates.add(merchant(parseString(parts[1])));
            }
        }


        return concatAnd(predicates);
    }

    public static Predicate<Transaction> concatAnd(List<Predicate<Transaction>> predicates) {
	    return predicates.stream()
                .reduce(x -> true, Predicate::and);
    }


    public static Predicate<Transaction> notReversal() {
	    return p -> !(p.getType().equals(TransactionType.REVERSAL));
    }

    public static Predicate<Transaction> fromDate(LocalDateTime localDateTime) {
	    return p -> p.getDate().isAfter(localDateTime);
    }

    public static Predicate<Transaction> toDate(LocalDateTime localDateTime) {
        return p -> p.getDate().isBefore(localDateTime);
    }

    public static Predicate<Transaction> merchant(String merchant) {
        return p -> p.getMerchant().equalsIgnoreCase(merchant);
    }
}

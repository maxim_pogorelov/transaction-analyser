package com.pogorelov.transaction.service;

import com.pogorelov.transaction.model.Transaction;
import com.pogorelov.transaction.model.TransactionType;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExtractDataServiceTest {

    private ExtractDataService extractDataService;

    @Before
    public void before() {
        extractDataService = new ExtractDataService();
    }

    @Test
    public void shouldExtract() {
        List<String> lines = Arrays.asList("WLMFRDGD, 20/08/2018 12:45:33, 59.99, Kwik-E-Mart, PAYMENT,",
                "YGXKOEIA, 20/08/2018 12:46:17, 10.95, Kwik-E-Mart, PAYMENT,",
                "LFVCTEYM, 20/08/2018 12:50:02, 5.00, MacLaren, PAYMENT,",
                "SUOVOISP, 20/08/2018 13:12:22, 5.00, Kwik-E-Mart, PAYMENT,",
                "AKNBVHMN, 20/08/2018 13:14:11, 10.95, Kwik-E-Mart, REVERSAL, YGXKOEIA",
                "JYAPKZFZ, 20/08/2018 14:07:10, 99.50, MacLaren, PAYMENT");
        List<Transaction> transactions = extractDataService.extractTransactions(lines);
        assertNotNull(transactions);
        assertEquals(6, transactions.size());
        assertEquals("WLMFRDGD", transactions.get(0).getId());
        assertEquals(TransactionType.PAYMENT, transactions.get(0).getType());
        assertEquals("AKNBVHMN", transactions.get(4).getId());
        assertEquals(TransactionType.REVERSAL, transactions.get(4).getType());

    }
}

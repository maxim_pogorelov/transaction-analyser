package com.pogorelov.transaction.service;

import com.pogorelov.transaction.model.Transaction;
import com.pogorelov.transaction.model.TransactionsStatistic;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static com.pogorelov.transaction.service.ExtractDataService.parseDate;
import static com.pogorelov.transaction.service.filter.PredicateService.concatAnd;
import static com.pogorelov.transaction.service.filter.PredicateService.fromDate;
import static com.pogorelov.transaction.service.filter.PredicateService.merchant;
import static com.pogorelov.transaction.service.filter.PredicateService.notReversal;
import static com.pogorelov.transaction.service.filter.PredicateService.toDate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProcessTransactionServiceTest {

	private ProcessTransactionService processTransactionService;
	private ExtractDataService extractDataService;

	@Mock
	private InputStreamService inputStreamService;

	@Before
	public void before() {
		when(inputStreamService.getLines(anyString()))
				.thenReturn(Arrays.asList("WLMFRDGD, 20/08/2018 12:45:33, 59.99, Kwik-E-Mart, PAYMENT,",
						"YGXKOEIA, 20/08/2018 12:46:17, 10.95, Kwik-E-Mart, PAYMENT,",
						"LFVCTEYM, 20/08/2018 12:50:02, 5.00, MacLaren, PAYMENT,",
						"SUOVOISP, 20/08/2018 13:12:22, 5.00, Kwik-E-Mart, PAYMENT,",
						"AKNBVHMN, 20/08/2018 13:14:11, 10.95, Kwik-E-Mart, REVERSAL, YGXKOEIA",
						"JYAPKZFZ, 20/08/2018 14:07:10, 99.50, MacLaren, PAYMENT"));

		extractDataService = new ExtractDataService();
		processTransactionService = new ProcessTransactionService(inputStreamService, extractDataService);
	}

	@Test
	public void shouldGenerateStatisticsNoFilter() {
		TransactionsStatistic transactionsStatistic = processTransactionService.process("/path", Objects::nonNull);
		assertNotNull(transactionsStatistic);
		assertEquals(6, (int) transactionsStatistic.getNumberOfTransactions());
		assertEquals(31.89, transactionsStatistic.getAverage(), 0.01);
	}

    @Test
    public void shouldGenerateStatisticsKwik() {
        TransactionsStatistic transactionsStatistic = processTransactionService.process("/path", merchant("Kwik-E-Mart"));
        assertNotNull(transactionsStatistic);
        assertEquals(4, (int) transactionsStatistic.getNumberOfTransactions());
        assertEquals(21.72, transactionsStatistic.getAverage(), 0.01);
    }

    @Test
    public void shouldGenerateStatisticsPaymentOnly() {
        TransactionsStatistic transactionsStatistic = processTransactionService.process("/path", notReversal());
        assertNotNull(transactionsStatistic);
        assertEquals(5, (int) transactionsStatistic.getNumberOfTransactions());
        assertEquals(36.08, transactionsStatistic.getAverage(), 0.01);
    }

    @Test
    public void shouldGenerateStatisticsMainFilter() {
	    List<Predicate<Transaction>> predicates = Arrays.asList(notReversal(), merchant("Kwik-E-Mart"),
                fromDate(parseDate("20/08/2018 12:00:00")), toDate(parseDate("20/08/2018 13:00:00")));

        TransactionsStatistic transactionsStatistic = processTransactionService.process("/path", concatAnd(predicates));
        assertNotNull(transactionsStatistic);
        assertEquals(2, (int) transactionsStatistic.getNumberOfTransactions());
        assertEquals(35.47, transactionsStatistic.getAverage(), 0);
    }
}

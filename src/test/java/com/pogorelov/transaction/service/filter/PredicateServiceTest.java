package com.pogorelov.transaction.service.filter;

import com.pogorelov.transaction.model.Transaction;
import org.junit.Before;
import org.junit.Test;

import java.util.function.Predicate;

import static org.junit.Assert.assertNotNull;

public class PredicateServiceTest {

    private PredicateService predicateService;

    @Before
    public void init() {
        predicateService = new PredicateService();
    }

    @Test
    public void shouldParseFilter() {
        Predicate<Transaction> filter = predicateService.buildFilter("", "fromDate: 20/08/2018 12:00:00",
                "toDate: 20/08/2018 13:00:00",
                "merchant: Kwik-E-Mart" );

        assertNotNull(filter);
    }
}
